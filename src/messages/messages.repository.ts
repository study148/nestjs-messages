import { Injectable } from "@nestjs/common";
import { readFile, writeFile } from "fs/promises";

const repositoryFile = 'messages.json';

@Injectable()
export class MessagesRepository {

  private async fetch() {
    const data = await readFile(repositoryFile, 'utf-8');
    return JSON.parse(data);
  }

  async findOne(id: string) {
    const data = await this.fetch();
    return data['messages'][id];
  }

  async findAll() {
    const data = await this.fetch();
    return data['messages'];
  }

  async create(content: string) {
    const data = await this.fetch();

    if (data['lastID'] === undefined) {
      data['lastID'] = 0;
    }
    if (data['messages'] === undefined) {
      data['messages'] = {};
    }

    const id = data['lastID'] + 1;
    data['lastID'] = id;

    const newMessage = { id, content };

    data['messages'][id.toString()] = newMessage;

    await writeFile('messages.json', JSON.stringify(data));

    return newMessage;
  }
}
